package fr.dawan.exemple.entitties;

import java.io.Serializable;
import java.util.Map;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
@Entity
public class Test implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private long id;
	
	private int version;
	
	@ManyToMany
	private Map<Integer,Article> a;
	
}
