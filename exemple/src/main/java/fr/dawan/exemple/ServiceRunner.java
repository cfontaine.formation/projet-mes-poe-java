package fr.dawan.exemple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import fr.dawan.exemple.dtos.ArticleDto;
import fr.dawan.exemple.services.ArticleService;

@Component
public class ServiceRunner implements CommandLineRunner {

	@Autowired
	ArticleService service;
	
	@Override
	public void run(String... args) throws Exception {

		service.findAll().forEach(System.out::println);
		
		System.out.println(service.findById(1L));
		
		ArticleDto a=new ArticleDto("memoire DDR4 16go",160.0);
		System.out.println(service.saveOrUpdate(a));
	}

}
