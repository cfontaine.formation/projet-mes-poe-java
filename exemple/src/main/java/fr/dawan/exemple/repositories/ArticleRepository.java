package fr.dawan.exemple.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.exemple.entitties.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {
	
	@Query(value="SELECT a FROM Article a WHERE a.prix<:prix")
	List<Article > findByPrixLessThan(double prix);
	
	@Query(value="SELECT a FROM Article a WHERE a.description LIKE :model")
	List<Article> findByDescriptionLike(@Param("model")String model);

}
