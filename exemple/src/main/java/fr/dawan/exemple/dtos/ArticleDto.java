package fr.dawan.exemple.dtos;

public class ArticleDto {
	
	private long id;
	
	private String description;
	
	private double prix;
	
	public ArticleDto() {

	}

	public ArticleDto(String description, double prix) {
		this.description = description;
		this.prix = prix;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "ArticleDto [id=" + id + ", description=" + description + ", prix=" + prix + "]";
	}

}
