package fr.dawan.exemple.services;

import java.util.List;

import fr.dawan.exemple.dtos.ArticleDto;

public interface ArticleService {
	
	List<ArticleDto> findAll();
	
	ArticleDto findById(long id);
	
	ArticleDto saveOrUpdate (ArticleDto article);
}
