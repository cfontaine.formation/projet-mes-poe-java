package fr.dawan.exemple.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.dawan.exemple.dtos.ArticleDto;
import fr.dawan.exemple.entitties.Article;
import fr.dawan.exemple.repositories.ArticleRepository;

@Service
public class ArticleServiceImpl implements ArticleService {
	
	@Autowired
	private ArticleRepository repository;
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public List<ArticleDto> findAll() {
		return repository.findAll().stream().map(a-> mapper.map(a,ArticleDto.class) ).toList();
	}

	@Override
	public ArticleDto findById(long id) {
		 return mapper.map(repository.findById(id).get(),ArticleDto.class);
	}

	@Override
	public ArticleDto saveOrUpdate(ArticleDto articleDto) {
		Article a=mapper.map(articleDto, Article.class);
		System.out.println(a);
		return mapper.map(repository.saveAndFlush(a),ArticleDto.class);
	}

}
