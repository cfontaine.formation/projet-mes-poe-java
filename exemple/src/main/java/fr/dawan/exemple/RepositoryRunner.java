package fr.dawan.exemple;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import fr.dawan.exemple.entitties.Article;
import fr.dawan.exemple.repositories.ArticleRepository;



//@Component
public class RepositoryRunner implements CommandLineRunner {
	
	@Autowired
	private ArticleRepository repo;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("repository runner");
		
		List <Article> lst=repo.findAll();
		for(Article a: lst) {
			System.out.println(a);
		}
		
		lst=repo.findByPrixLessThan(30.0);
		for(Article a: lst) {
			System.out.println(a);
		}
		
		
		lst=repo.findByDescriptionLike("d__%");
		for(Article a: lst) {
			System.out.println(a);
		}
	}

}
